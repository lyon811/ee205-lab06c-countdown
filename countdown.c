///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Years: 6 Days: 4 Hours: 19 Minutes: 33 Seconds: 53
//   Years: 6 Days: 4 Hours: 19 Minutes: 33 Seconds: 54
//
// @author Lyon Singleton lyonws@hawiai.edu
// @date   02/23/2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <unistd.h>
#define YEAR 2015 - 1900
#define HOUR 3
#define MIN 43
#define SEC 21
#define MONTH 4
#define DAY 23
#define WDAY 6
#define DAYLIGHT_SAVINGS 0

int main()
{
    struct tm times;

    times.tm_year = YEAR;
    times.tm_hour = HOUR;
    times.tm_min = MIN;
    times.tm_sec = SEC;
    times.tm_mon = MONTH;
    times.tm_mday = DAY;
    times.tm_wday = WDAY;
    times.tm_isdst = DAYLIGHT_SAVINGS;
    printf("Reference time: %s\n", asctime(&times));

    while (true)
    {
        struct tm tm = *localtime(&(time_t){time(NULL)});

        time_t TimeT1 = mktime(&tm);
        time_t TimeT2 = mktime(&times);

        time_t diff = difftime(TimeT1, TimeT2);

        int seconds = diff % 60;
        diff /= 60;
        int minutes = diff % 60;
        diff /= 60;
        int hours = diff % 24;
        diff /= 24;
        int days = diff % 365;
        diff /= 365;
        int years = diff;

        printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n", years, days, hours, minutes, seconds);
        sleep(1);
    };
    return 0;
}
